var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var header = require('gulp-header');
var gulpif = require('gulp-if');
var cssnano = require('gulp-cssnano');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var pkg = require('./package.json');
var connect = require('gulp-connect');
var wiredep = require('wiredep').stream;
var inject = require('gulp-inject');
var useref = require('gulp-useref');
var del = require('del');
var proxy = require('http-proxy-middleware');
var url = require('url');
var htmlmin = require('gulp-htmlmin');
var runSequence = require('run-sequence');
var bower = require('gulp-bower');
var rev = require('gulp-rev');
var revReplace = require('gulp-rev-replace');
var filter = require('gulp-filter');

var config = require('./gulp/config');

gulp.task('sass', function () {
    return gulp.src(config.app + 'app/styles/**/*.scss')
            .pipe(sass.sync().on('error', sass.logError))
            .pipe(gulp.dest(config.app + 'styles'));
});

gulp.task('browserSync', function () {

    var apiProxy = proxy('/api', {target: 'http://localhost:8080/', changeOrigin: true});
    browserSync.init({
		port:8080,
        server: {
            baseDir: [config.app, config.app + 'app', config.app + '.tmp', config.app + 'bower_components'],
            routes: {
                '/bower_components': 'bower_components/'
            },
            middleware: [apiProxy]
        }
    });
});

gulp.task('wiredep', () => {
    gulp.src(config.app + 'index.html')
            .pipe(wiredep({
                directory: config.app + 'bower_components'
            }))
            .pipe(gulp.dest(config.app));
});

gulp.task('dev', ['bower', 'browserSync', 'icons', 'sass'], function () {
    gulp.watch(config.app + 'app/styles/*.scss', ['sass']);
    gulp.watch(config.app + 'app/**/*.js');
    gulp.watch(config.app + 'app/images/*.**');

    gulp.watch([config.app + '*.html', config.app + 'app/**']).on('change', browserSync.reload);

});

gulp.task('useref', function () {
//    var indexHtmlFilter = filter(['**/*', '!**/index.html'], {restore: true});
    var cssAndJsFilter = filter(['**/*.js', '**/*.css'], {restore: true});
    return gulp.src(config.app + '*.html')
            .pipe(useref())
            .pipe(gulpif('*.js', uglify()))
            .pipe(gulpif('*.css', cssnano({zindex: false})))
            .pipe(cssAndJsFilter)
            .pipe(rev())
            .pipe(cssAndJsFilter.restore)
            .pipe(revReplace())
            .pipe(gulp.dest(config.dist));
});


gulp.task('clean', function () {
    del.sync(config.app + "fonts");
    del.sync(config.app + "styles");
    del.sync(config.dist);
});

gulp.task('icons', function () {
    gulp.src(config.app + 'bower_components/font-awesome/fonts/**.*')
            .pipe(gulp.dest(config.app + 'fonts'));
    gulp.src(config.app + 'bower_components/bootstrap-sass/assets/fonts/bootstrap/**.*')
            .pipe(gulp.dest(config.app + 'fonts/bootstrap'));
});

gulp.task('build', function (callback) {
    runSequence('clean', 'sass', 'wiredep', 'useref', 'copy', 'minify', 'icons');
});

gulp.task('copy', function () {
    gulp.src(config.app + 'bower_components/font-awesome/fonts/**.*')
            .pipe(gulp.dest(config.dist + 'fonts'));
    gulp.src(config.app + 'bower_components/bootstrap-sass/assets/fonts/bootstrap/**.*')
            .pipe(gulp.dest(config.dist + 'fonts/bootstrap'));
    gulp.src(config.app + 'images/**.*')
            .pipe(gulp.dest(config.dist + 'images'));
    gulp.src(config.app + 'META-INF/**.*')
            .pipe(gulp.dest(config.dist + 'META-INF'));
    gulp.src(config.app + 'WEB-INF/**.*')
            .pipe(gulp.dest(config.dist + 'WEB-INF'));
});

gulp.task('minify', function () {
    gulp.src(config.app + 'app/**/*.html')
            .pipe(htmlmin({collapseWhitespace: true}))
            .pipe(gulp.dest(config.dist + 'app'));
});

gulp.task('bower', function () {
    return bower({cmd: 'update'});
});
