package com.srm.test.controller;

import com.srm.test.domain.enumerated.RiskType;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(RiskTypeController.class)
public class RiskTypeControllerTest {
    
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getRiskType_shouldGetEnum() throws Exception {

        MvcResult result = mockMvc.perform(get("/api/risk-types")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();
        
        String content = result.getResponse().getContentAsString();
        
        for (RiskType riskType : RiskType.values()) {
            Assert.assertThat(content, CoreMatchers.containsString(riskType.name()));
        }
        
    }

}
