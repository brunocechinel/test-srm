package com.srm.test.controller;

import com.srm.test.domain.enumerated.RiskType;
import com.srm.test.dto.CreditAnalysisDTO;
import com.srm.test.service.CreditAnalysisService;
import com.srm.test.util.TestUtil;
import java.math.BigDecimal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(CreditAnalysisController.class)
public class CreditAnalysisControllerTest {

    private static final String DEFAULT_NAME = "Bruno";
    private static final BigDecimal DEFAULT_CREDIT_LIMIT = BigDecimal.TEN;
    private static final RiskType DEFAULT_RISK = RiskType.B;
    
    @Autowired
    private MockMvc mockMvc;
    
    @MockBean
    private CreditAnalysisService service;

    private CreditAnalysisDTO creditAnalysisDTO;
     
    @Before
    public void setUp() {
        creditAnalysisDTO = new CreditAnalysisDTO();
        creditAnalysisDTO.setName(DEFAULT_NAME);
        creditAnalysisDTO.setCreditLimit(DEFAULT_CREDIT_LIMIT);
        creditAnalysisDTO.setRisk(DEFAULT_RISK);
    }

    @Test
    public void postCreditAnalysis_shouldGetCreated() throws Exception {

        mockMvc.perform(post("/api/credit-analysis")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJson(creditAnalysisDTO)))
                .andExpect(status().isCreated())
                .andReturn();
        
    }
    
    @Test
    public void postCreditAnalysis_shouldGetBadRequest() throws Exception {

        creditAnalysisDTO.setName(null);
        
        mockMvc.perform(post("/api/credit-analysis")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJson(creditAnalysisDTO)))
                .andExpect(status().isBadRequest())
                .andReturn();
        
    }

}
