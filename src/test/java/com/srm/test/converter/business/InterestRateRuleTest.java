package com.srm.test.converter.business;

import com.srm.test.domain.enumerated.RiskType;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class InterestRateRuleTest {
    
    private List<RiskType> risks;
    
     @Before
     public void setUp() {
         risks = Arrays.asList(RiskType.values());
     }
    
    @Test
    public void calcInterestRate() {
        risks.forEach((riskType) -> {
            Assert.assertEquals(riskType.getInterestRateRule().calcInterestRate(), riskType.calcInterestRate());
        });
    }
}
