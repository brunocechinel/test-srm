package com.srm.test.converter;

import com.srm.test.converter.impl.CreditAnalysisConverter;
import com.srm.test.domain.entity.CreditAnalysisEntity;
import com.srm.test.domain.enumerated.RiskType;
import com.srm.test.dto.CreditAnalysisDTO;
import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { CreditAnalysisConverter.class })
public class ConverterTest {
    
    private static final String DEFAULT_NAME = "Bruno";
    private static final BigDecimal DEFAULT_CREDIT_LIMIT = BigDecimal.ZERO;
    private static final RiskType DEFAULT_RISK = RiskType.A;
    
    @Autowired
    private Converter<CreditAnalysisDTO, CreditAnalysisEntity> converter;
     
    private CreditAnalysisDTO creditAnalysisDTO;
     
    @Before
    public void setUp() {
        creditAnalysisDTO = new CreditAnalysisDTO();
        creditAnalysisDTO.setName(DEFAULT_NAME);
        creditAnalysisDTO.setCreditLimit(DEFAULT_CREDIT_LIMIT);
        creditAnalysisDTO.setRisk(DEFAULT_RISK);
    }
     
    @Test
    public void convertFromCreditAnalysisDTO_showdGetCreditAnalysisEntity() {
        CreditAnalysisEntity creditAnalysis = converter.convertFrom(creditAnalysisDTO);
        Assert.assertEquals(DEFAULT_NAME, creditAnalysis.getClient().getName());
        Assert.assertEquals(DEFAULT_CREDIT_LIMIT, creditAnalysis.getCreditLimit());
        Assert.assertEquals(DEFAULT_RISK.calcInterestRate(), creditAnalysis.getInterestRate());
        Assert.assertEquals(DEFAULT_RISK, creditAnalysis.getRisk());
    }
}
