(function () {
    'use strict';
    var appServices = angular.module('appServices');

    appServices.factory('RiskTypeService', ['$resource',
        function ($resource) {
            var resourceUrl = 'api';

            return $resource(resourceUrl, {}, {
                'findall': {
                    url: resourceUrl + "/risk-types",
                    method: 'GET',
                    isArray: true,
                    transformResponse: function (data) {
                        if (data) {
                            data = angular.fromJson(data);
                        }
                        return data;
                    }
                }
            });
        }]);

})();