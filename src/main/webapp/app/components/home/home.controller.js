(function () {
    'use strict';
    angular.module('test')
            .controller('HomeController', ['$scope', 'RiskTypeService', 'CreditAnalysisService',
                function ($scope, RiskTypeService, CreditAnalysisService) {

                    $scope.creditAnalysis = {};
                    $scope.saveCredit = function() {
                        CreditAnalysisService.save($scope.creditAnalysis,
                                function (data) {
                                    swal("Sucesso!", "Análise salva", "success");
                                    delete $scope.creditAnalysis;
                                },
                                function (error) {
                                            
                                    var errorMessages = [];
                                    angular.forEach(error.data.fieldErrors, function(value) {
                                      this.push("\n" + value.message);
                                    }, errorMessages);
                                    
                                    swal("Erro!", errorMessages, "error");
                                }
                        );
                    };

                    RiskTypeService.findall({},
                        function (data) {
                            $scope.riskTypes = data;
                        },
                        function (error) {
                            alert(JSON.stringify(error));
                            console.log("Error risk types");
                        }
                        );

                }
            ]);
})();