(function () {
    'use strict';
    var appServices = angular.module('appServices');

    appServices.factory('CreditAnalysisService', ['$resource',
        function ($resource) {
            var resourceUrl = 'api';

            return $resource(resourceUrl, {}, {
                'save': {
                    url: resourceUrl + "/credit-analysis",
                    method: 'POST',
                    isArray: false,
                    transformResponse: function (data) {
                        if (data) {
                            data = angular.fromJson(data);
                        }
                        return data;
                    }
                }
            });
        }]);

})();