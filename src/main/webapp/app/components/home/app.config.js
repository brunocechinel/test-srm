(function () {
    'use strict';
    angular
        .module('test')
        .config(['$urlRouterProvider', '$stateProvider', function ($urlRouterProvider, $stateProvider) {
            $urlRouterProvider.otherwise("/app");
            $stateProvider
                .state('app', {
                    url: '/app',
                    templateUrl: function () {
                        return "app/components/home/app.html";
                    },
                    controller: 'HomeController'
                });
        }])
})();