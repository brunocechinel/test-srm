(function () {
    'use strict';
    angular.module('appServices', ['ngResource']);
    angular
        .module('test', [
            'ui.router',
            'ui.bootstrap',
            'ui.select',
            'appServices',
        ]);
})();

