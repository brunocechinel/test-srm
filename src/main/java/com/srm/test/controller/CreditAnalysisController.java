package com.srm.test.controller;

import com.srm.test.domain.entity.CreditAnalysisEntity;
import com.srm.test.dto.CreditAnalysisDTO;
import com.srm.test.service.CreditAnalysisService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(value = "api/", produces = MediaType.APPLICATION_JSON_VALUE)
public class CreditAnalysisController {
    
    @Autowired
    private CreditAnalysisService creditAnalysisService;
    
    @PostMapping("/credit-analysis")
    public ResponseEntity<CreditAnalysisEntity> create(@Valid @RequestBody CreditAnalysisDTO creditAnalysisDTO) {
        CreditAnalysisEntity creditAnalysis = creditAnalysisService.create(creditAnalysisDTO);
        return new ResponseEntity<>(creditAnalysis, HttpStatus.CREATED);
    }
    
}
