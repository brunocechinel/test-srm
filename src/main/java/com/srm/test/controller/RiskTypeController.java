package com.srm.test.controller;

import com.srm.test.domain.enumerated.RiskType;
import java.util.Arrays;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
@RequestMapping(value = "api/", produces = MediaType.APPLICATION_JSON_VALUE)
public class RiskTypeController {
    
    @GetMapping("/risk-types")
    public ResponseEntity<List<RiskType>> getAll() {
        return ResponseEntity.ok(Arrays.asList(RiskType.values()));
    }
}
