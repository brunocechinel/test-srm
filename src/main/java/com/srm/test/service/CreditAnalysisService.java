package com.srm.test.service;

import com.srm.test.domain.entity.CreditAnalysisEntity;
import com.srm.test.dto.CreditAnalysisDTO;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface CreditAnalysisService {
    
    CreditAnalysisEntity create(CreditAnalysisDTO crediAnalysisDTO);
}
