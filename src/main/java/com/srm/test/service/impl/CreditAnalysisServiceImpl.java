package com.srm.test.service.impl;

import com.srm.test.converter.Converter;
import com.srm.test.domain.entity.CreditAnalysisEntity;
import com.srm.test.domain.entity.repository.CreditAnalysisRepository;
import com.srm.test.dto.CreditAnalysisDTO;
import com.srm.test.service.CreditAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreditAnalysisServiceImpl implements CreditAnalysisService {
    
    @Autowired
    private CreditAnalysisRepository creditAnalysisRepository;
    
    @Autowired
    private Converter<CreditAnalysisDTO, CreditAnalysisEntity> converter;
    
    @Override
    public CreditAnalysisEntity create(CreditAnalysisDTO crediAnalysisDTO) {
        CreditAnalysisEntity creditAnalysis = converter.convertFrom(crediAnalysisDTO);
        
        return creditAnalysisRepository.save(creditAnalysis);
    }
    
}
