package com.srm.test.domain.enumerated;

import com.srm.test.domain.entity.business.InterestRateRule;
import com.srm.test.domain.entity.business.impl.InterestRateRuleA;
import com.srm.test.domain.entity.business.impl.InterestRateRuleB;
import com.srm.test.domain.entity.business.impl.InterestRateRuleC;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum RiskType {

    A(new InterestRateRuleA()),
    B(new InterestRateRuleB()),
    C(new InterestRateRuleC());
    
    private final InterestRateRule interestRateRule;
    
    public InterestRateRule getInterestRateRule() {
        return this.interestRateRule;
    }
    
    public BigDecimal calcInterestRate() {
        return this.interestRateRule.calcInterestRate();
    }
}
