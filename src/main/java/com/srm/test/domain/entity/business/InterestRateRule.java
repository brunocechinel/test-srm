package com.srm.test.domain.entity.business;

import java.math.BigDecimal;

public interface InterestRateRule {

    BigDecimal calcInterestRate();
    
}
