package com.srm.test.domain.entity;

import com.srm.test.domain.enumerated.RiskType;
import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "CREDIT_ANALYSIS")
public class CreditAnalysisEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    
    @Column(name = "CREDIT_LIMIT")
    private BigDecimal creditLimit;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "RISK")
    private RiskType risk;
    
    @Column(name = "INTEREST_RATE")
    private BigDecimal InterestRate;
    
    @JoinColumn(name = "CLIENT_ID")
    @ManyToOne(cascade = CascadeType.ALL)
    private ClientEntity client;
}
