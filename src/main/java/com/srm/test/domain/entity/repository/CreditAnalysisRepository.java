package com.srm.test.domain.entity.repository;

import com.srm.test.domain.entity.CreditAnalysisEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CreditAnalysisRepository extends JpaRepository<CreditAnalysisEntity, Long> {
    
}
