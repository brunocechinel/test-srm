package com.srm.test.domain.entity.business.impl;

import com.srm.test.domain.entity.business.InterestRateRule;
import java.math.BigDecimal;

public class InterestRateRuleC implements InterestRateRule {

    @Override
    public BigDecimal calcInterestRate() {
        return new BigDecimal("0.2");
    }
    
}
