package com.srm.test.converter.impl;

import com.srm.test.domain.entity.CreditAnalysisEntity;
import com.srm.test.dto.CreditAnalysisDTO;
import com.srm.test.converter.Converter;
import com.srm.test.domain.entity.ClientEntity;
import org.springframework.stereotype.Service;

@Service
public class CreditAnalysisConverter implements Converter<CreditAnalysisDTO, CreditAnalysisEntity> {
    
    @Override
    public CreditAnalysisEntity convertFrom(CreditAnalysisDTO dto) {
        CreditAnalysisEntity creditAnalysis = new CreditAnalysisEntity();
        
        creditAnalysis.setRisk(dto.getRisk());
        creditAnalysis.setCreditLimit(dto.getCreditLimit());
        creditAnalysis.setInterestRate(dto.getRisk().calcInterestRate());
        creditAnalysis.setClient(new ClientEntity(dto.getName()));
        
        return creditAnalysis;
    }
}
