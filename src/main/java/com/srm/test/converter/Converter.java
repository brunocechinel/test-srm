package com.srm.test.converter;

public interface Converter<T, E> {
    
    public E convertFrom(T dto);
}
