package com.srm.test.dto;

import com.srm.test.domain.enumerated.RiskType;
import java.math.BigDecimal;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class CreditAnalysisDTO {

    @NotNull(message = "Campo nome não pode ser nulo")
    @Size(max = 64, message = "Campo nome não pode conter mais de 64 caracteres")
    private String name;
    
    @NotNull(message = "Campo limite de crédito não pode ser nulo")
    @Min(value = 0, message = "Campo limite de crédito deve ser maior que 0")
    private BigDecimal creditLimit;
    
    @NotNull(message = "Campo risco não pode ser nulo")
    private RiskType risk;
    
}
