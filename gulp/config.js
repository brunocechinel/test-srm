'use strict';

module.exports = {
    app: 'src/main/webapp/',
    dist: 'target/www/',
    swaggerDist: 'target/www/swagger-ui/',
    test: 'src/test/javascript/',
    bower: 'src/main/webapp/bower_components/',
    tmp: 'target/tmp',       
    liveReloadPort: 35729,
    uri: 'http://localhost:'
    
};