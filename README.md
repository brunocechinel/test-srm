# README.md

Ambiente

* **Maven 3**
* **Java 8**
* **[Lombok](https://projectlombok.org/)**

FrontEnd

* **Bower**
* **Gulp**

```
bower install
gulp dev
```

Swagger disponivel em:

```
/swagger-ui
```

Banco H2 disponivel em:

```
/console
```